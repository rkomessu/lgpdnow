import 'foundation-sites/dist/js/plugins/foundation.tabs'

export default {
  init() {
    $(document).foundation();
    // eslint-disable-next-line
    var elem = new Foundation.Tabs(element, options);
  },
  // JavaScript to be fired on all pages, after page specific JS is fired
  finalize() {
  },
};
