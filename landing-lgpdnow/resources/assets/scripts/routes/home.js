import 'slick-carousel';
export default {
  init() {
   
    // $('.three-block-slider').slick({
    //   dots: true,
    //   infinite: true,
    //   speed: 300,
    //   arrows: false,
    //   autoplay: true,
    //   autoplaySpeed: 3000,
    //   slidesToShow: 3,
    //   slidesToScroll: 3,
    //   responsive: [
    //     {
    //       breakpoint: 1024,
    //       settings: {
    //         slidesToShow: 2,
    //         slidesToScroll: 2,
    //       },
    //     },
    //     {
    //       breakpoint: 639,
    //       settings: {
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //       },
    //     },
    //   ],
    // });

    $(window).scroll(function(){
      var scroll = $(window).scrollTop();
      if(scroll > $('.mega-menu').innerHeight()) {
        $('.mega-menu').addClass('pinned');
      } else {
        $('.mega-menu').css('transform', 'translateY(-'+scroll+'px)');
        $('.mega-menu').removeClass('pinned');
      }
    })
   
  },
  // JavaScript to be fired on all pages, after page specific JS is fired
  finalize() {
  },
};
