
import 'foundation-sites/dist/js/plugins/foundation.accordionMenu'
import 'foundation-sites/dist/js/plugins/foundation.core'
import 'foundation-sites/dist/js/plugins/foundation.util.keyboard'
import 'foundation-sites/dist/js/plugins/foundation.util.nest'
// import 'jquery-mask-plugin';
import 'slick-carousel';

export default {
  init() {
    $(document).foundation();
    $('.menu-overlay').on('click', function(){
      $('.menu-control').removeClass('active');
      $('.main__menu').removeClass('hidden');
      $('.main').removeClass('expanded');
      $('.menu-overlay').removeClass('show');
    });
    $('.menu-control').on('click', function(){
      event.preventDefault();
      $(this).toggleClass('active');
      $('.main__menu').toggleClass('hidden');
      $('.main').toggleClass('expanded');
      $('.menu-overlay').toggleClass('show');
    });
    
    $('.accordion__title').on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      $(this).next().toggleClass('show');
      // $('html, body').animate({ scrollTop: $(this).offset().top-20 }, 400);
    });
    $('.bt-close').on('click', function (e) {
      e.preventDefault();
      $(this).parent().remove();
    });

    $('.tabs-options ul li').on('click', function (e) {
      e.preventDefault();
      if(!$(this).hasClass('active')) {
        var c = $(this).attr('class');
        $('.tabs-options ul li').removeClass('active');
        $('.tabs-content > div').removeClass('show');
        $(this).parent().parent().find('.tabs-content #'+c).addClass('show');
        $(this).addClass('active');
      }
    });

    $('.menu-search').on('click', function (e) {
      e.preventDefault();
      $('.menu-search-input').toggleClass('show');
      document.getElementById('search-menu').focus();
    });

    $('.carousel').slick({
      dots: true,
      infinite: false,
      speed: 500,
      arrows: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
    
    $('.tooltip-action').on('click', function (e) {
      e.preventDefault();
    });
  },
  // JavaScript to be fired on all pages, after page specific JS is fired
  finalize() {
  },
};
