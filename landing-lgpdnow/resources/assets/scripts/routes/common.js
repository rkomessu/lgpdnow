
import 'foundation-sites/dist/js/plugins/foundation.accordionMenu'
import 'foundation-sites/dist/js/plugins/foundation.core'
import 'foundation-sites/dist/js/plugins/foundation.util.keyboard'
import 'foundation-sites/dist/js/plugins/foundation.util.nest'
// import 'jquery-mask-plugin';

export default {
  init() {

    // $('#phone_number').mask('00 0000 0000');


    $('.input__submit').on('click', function(){
      event.preventDefault();
      if($('#form_username').val() == ''){
        $('#form_username').parent().addClass('is_empty');
        $('#form_username').parent().find('.input-error').addClass('show');
      }
      if($('#form_password').val() == '') {
        $('#form_password').parent().addClass('is_empty');
        $('#form_password').parent().find('.input-error').addClass('show');
      }
    });

      //input live search
    // $('.search-block__form input').on('input', function () {
    //   if ($(this).val().length > 0) {
    //     $('.search-block__result').slideDown();
    //   } else {
    //     $('.search-block__result').slideUp();
    //   }
    // });

    $('.mega-menu__nav .submenu').click( function () {
      $('.mega-menu__element-right').toggleClass('show');
    });
    $('.mega-menu__burger').click( function () {
      $(this).toggleClass('show');
      $('.mega-menu__bg').toggleClass('opened');
      $('.mega-menu__nav').toggleClass('show');
      $('.mega-menu__close').toggleClass('mega-menu__close-active');
    });

    // eslint-disable-next-line no-unused-vars,no-undef
    var elem = new Foundation.AccordionMenu($('.accordion-menu'));

    // $('.testimonial-slider').slick({
    //   dots: true,
    //   infinite: true,
    //   speed: 500,
    //   arrows: false,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   autoplay: true,
    //   autoplaySpeed: 5000,
    // });
    
    // $('.mega-menu__search-icon').on('click', function(){
    //   $('.mega-menu__search-box').slideToggle();
    //   $('.mega-menu__search-box form input#search').focus();
    // });
  },
  // JavaScript to be fired on all pages, after page specific JS is fired
  finalize() {
  },
};
