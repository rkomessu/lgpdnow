# WebPack split v4.1
Install project components - yarn install

## Install

Development requires an environment
**node - v12.16.1**
**npm - v6.13.4**
**yarn - v1.22**


Install with yarn:

```bash
sudo yarn install
```

## Start

Install with yarn:

```bash
yarn start
```

## Build project

Install with yarn:
```bash
yarn build
```
